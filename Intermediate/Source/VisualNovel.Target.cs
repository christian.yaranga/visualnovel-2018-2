using UnrealBuildTool;

public class VisualNovelTarget : TargetRules
{
	public VisualNovelTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("VisualNovel");
	}
}
